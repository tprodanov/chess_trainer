import chess
import chess.polyglot
import pickle


class Opening:
    def __init__(self, code, name, moves):
        self.code = code
        self.name = name
        self.board = chess.Board()
        for i, move in enumerate(moves.split()):
            if i % 3 == 0:
                continue
            try:
                self.board.push_san(move)
            except ValueError as e:
                print(code, name, moves, sep='\n')
                print(self.board)
                print(move)
                raise e
        self.zobrist_hash = chess.polyglot.zobrist_hash(self.board)

    def __str__(self):
        return self.code + '. ' + self.name

    def half_moves(self):
        return len(self.board.move_stack)


class OpeningBook:
    def __init__(self, f):
        self.openings = []
        self.positions = {}
        for line in f:
            code = line.strip()
            moves = next(f).strip()
            name = next(f).strip()
            if next(f).strip():
                print('Non empty line after', code, name, moves, sep='\n')
                raise ValueError

            opening = Opening(code, name, moves)
            self.openings.append(opening)
            h = opening.zobrist_hash
            if h not in self.positions:
                self.positions[h] = []
            self.positions[h].append(opening)

    def find_all(self, board):
        return self.positions[chess.polyglot.zobrist_hash(board)]

    def find_by_name(self, name, min_score=4):
        scores = []
        for opening in self.openings:
            scores.append((opening, local_alignment(name.lower(), opening.name.lower())))
        scores.sort(key=lambda tup: (-tup[1], tup[0].half_moves()))

        if not scores or scores[0][1] < min_score:
            return []
        min_score = max(min_score, min(int(scores[0][1] * .95), scores[0][1] - 1))
        res = []
        for opening, score in scores:
            if score >= min_score:
                res.append(opening)
            else:
                break
        return res

    def dump(self, pickle_filename):
        with open(pickle_filename, 'wb') as outp:
            pickle.dump(self, outp)

    @staticmethod
    def load(pickle_filename):
        with open(pickle_filename, 'rb') as inp:
            return pickle.load(inp)


def local_alignment(str1, str2, mismatch=-.3, gap=-.3):
    n = len(str1) + 1
    m = len(str2) + 1
    score = [[0] * m for _ in range(n)]
    best = 0
    
    for i in range(1, n):
        for j in range(1, m):
            score[i][j] = max(score[i - 1][j - 1] + 1 
                                    if str1[i - 1] == str2[j - 1] else mismatch,
                              score[i - 1][j] + gap,
                              score[i][j - 1] + gap,
                              0)
            best = max(best, score[i][j])
    return best

