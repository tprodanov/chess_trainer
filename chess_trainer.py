import chess
import chess.svg
import chess.uci
import IPython.display as displ
import termcolor


engine = chess.uci.popen_engine('stockfish-10-linux/Linux/stockfish_10_x64')


def draw(board, lastmove, clear=True):
    if clear:
        displ.clear_output()
    displ.display(displ.SVG(chess.svg.board(board, lastmove=lastmove, size=400)))


def play(positions_generator):
    while True:
        board = next(positions_generator)
        engine.stop()
        engine.ucinewgame()

        info_handler = chess.uci.InfoHandler()
        engine.info_handlers.append(info_handler)
        draw(board, None)
        summaries = []
        history = []

        while True:
            engine.stop()
            engine.position(board)
            engine.go(depth=30, async_callback=True)

            inp = input()
            if inp == 'q':
                engine.stop()
                return
            if inp == 'next':
                break
            if inp == 't':
                board.pop()
                summaries.pop()
                history.pop()
                draw(board, board.peek() if board.move_stack else None)
                continue
            if inp == 'show':
                if summaries:
                    print(summaries[-1])
                else:
                    print('No summaries available')
                continue
            if inp == 'history':
                print_history(history, board.turn)
                continue

            try:
                move = board.parse_san(inp)
            except ValueError:
                print(inp, 'is not legal')
                continue

            history.append(inp)
            engine.stop()

            board.push(move)
            draw(board, move)
            board.pop()

            improv, summary = compare_with_best(board, move, engine, info_handler)
            summaries.append(summary)
            print(termcolor.colored('Improvement %.2f' % improv, None if improv < 2 else 'red'))
            board.push(move)
            

def score_to_str(info_handler):
    score = info_handler.info['score'][1]
    if score.mate is None:
        res = '%+.2f' % (score.cp / 100)
    else:
        res = 'M%+d' % score.mate
    return res + '  (depth: %d)' % info_handler.info['depth']


def improvement(score1, score2, white=True):
    mult = -1 # 1 if white else -1

    if score1.mate is not None:
        if mult * score1.mate > 0:
            if score2.mate is None or mult * score2.mate < 0:
                return 1000
        return mult * score2.mate - mult * score1.mate

    if score2.mate is not None:
        if mult * score2.mate > 0 and score1.mate is None:
            print('Score 1 does not see a mate')
            return 0
        return 1000

    return mult / 100 * (score1.cp - score2.cp)


def compare_with_best(board, move, engine, info_handler):
    best = info_handler.info['pv'][1][0]
    best_san = board.san(best)
    move_san = board.san(move)

    board.push(best)
    engine.position(board)
    engine.go(movetime=500)
    score1 = info_handler.info['score'][1]
    summary = 'Best %5s,   %s\n' % (best_san, score_to_str(info_handler))
    board.pop()

    if best_san == move_san:
        summary += 'Used %5s\n' % move_san
        return 0, summary

    board.push(move)
    engine.position(board)
    engine.go(movetime=500)
    score2 = info_handler.info['score'][1]
    summary += 'Used %5s,   %s\n' % (move_san, score_to_str(info_handler))
    board.pop()

    improv = improvement(score1, score2, board.turn)
    return improv, summary


def print_history(history, curr_turn):
    turn = (curr_turn + len(history)) % 2 == 1
    turn_num = 1
    if not turn:
        print('1.  ... ', end='   ')
    for move in history:
        if turn:
            print('%d. %-5s' % (turn_num, move), end='   ')
        else:
            print('%-5s' % move)
            turn_num += 1
        turn = not turn
    if not turn:
        print()


def starting():
    while True:
        yield chess.Board()

def one_pawn():
    while True:
        board = chess.Board()
        board = chess.Board()
        board.clear_board()
        #board.set_piece_at(chess.E4, chess.Piece(chess.PAWN, chess.WHITE))
        #board.set_piece_at(chess.E6, chess.Piece(chess.KING, chess.WHITE))
        #board.set_piece_at(chess.E8, chess.Piece(chess.KING, chess.BLACK))
        board.set_piece_at(chess.E5, chess.Piece(chess.PAWN, chess.BLACK))
        board.set_piece_at(chess.E3, chess.Piece(chess.KING, chess.BLACK))
        board.set_piece_at(chess.E1, chess.Piece(chess.KING, chess.WHITE))
        board.turn = chess.BLACK
        yield board


if __name__ == '__main__':
    main()

